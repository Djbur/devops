TERRAFORM Infra

This Architecture with Terraform configuration file for provisioning a PHP application infrastructure on Google Cloud Platform (GCP). It defines the following resources:

A google_compute_network resource for creating a custom VPC network named "vpc-php" with auto_create_subnetworks set to false.
A google_compute_subnetwork resource for creating a subnet named "php-subnet" within the VPC network with an IP CIDR range of 10.0.0.0/24 and a secondary IP range of 192.168.0.0/24.
A google_compute_firewall resource named "allow-http" that allows inbound TCP traffic on port 80 from any source IP address.
A google_container_cluster resource for creating a Kubernetes cluster named "php-cluster" with a custom node pool named "php-node-pool" that uses preemptible VM instances. The cluster is configured to use the VPC network and subnet created earlier.
A google_compute_firewall resource named "allow-ssh" that allows inbound TCP traffic on port 22 from a specific source IP address.
A google_compute_firewall resource named "allow-http-from-ips" that allows inbound TCP traffic on port 80 from a specific list of source IP addresses.
A google_compute_firewall resource named "allow-https" that allows inbound TCP traffic on port 443 from any source IP address.
Note that some values, such as the project ID, region, zone, machine type, node count, and disk size are specified as variables and need to be replaced with actual values.

## To execute the terraform init command and download the necessary plugins for the Google provider, follow these steps ##

Save the code in a file named main.tf.

Open a terminal in the directory where the main.tf file is located.

Make sure you have logged in to the Google Cloud Platform account you want to use. To do this, run the following command:

Copy code
gcloud auth login
Export the Google Cloud Platform credentials as environment variables on your system. Run the following command in the terminal:

javascript
Copy code
export GOOGLE_APPLICATION_CREDENTIALS="/path/to/your/credentials.json"
Make sure to replace "/path/to/your/credentials.json" with the path on your system where the credentials JSON file you downloaded from GCP is located.

Export the IP address you want to use to access the instance. Run the following command in the terminal:

javascript
Copy code
export TF_VAR_ip_address="<ip-address>"
Replace "<ip-address>" with the IP address you want to use.

Run the following command to initialize Terraform in the current directory:

csharp
Copy code
terraform init
This command will download the necessary plugins for the Google Cloud Platform provider.

Finally, run the following command to apply the changes defined in the main.tf file:

Copy code
terraform apply
This command will create the necessary resources in GCP as defined in the main.tf file.


## CI/CD ##

This GitLabCI pipeline consists of several stages, each representing a step in the Continuous Integration/Continuous Deployment (CI/CD) process.

The first stage is the build stage, which uses the docker image to build the PHP application container image. The Docker image is built using the docker build command, and then tagged with the IMAGE_TAG variable, which is set to the SHA of the current Git commit. After the image is built, it is pushed to a Docker registry using docker push.

The test stage is the second stage, which uses the alpine image to run automated tests on the application. The commands for running the tests are not specified in the pipeline.

The third stage, analyze, uses the sonarsource/sonar-scanner-cli image to analyze the code for quality issues. The sonar-scanner command is used to perform the analysis, and the SONAR_HOST_URL and SONAR_LOGIN_TOKEN variables are used to specify the location of the SonarQube server and the login credentials.

The fourth stage, configure_kubectl, is used to set up the kubectl command-line tool to access the Kubernetes cluster. The google/cloud-sdk image is used to run the gcloud command to get the credentials for the Kubernetes cluster.

The fifth and final stage is the deploy stage, which deploys the application to the Kubernetes cluster. The helm command is used to add the ingress-nginx repository, and then to install the ingress-nginx chart. Finally, the kubectl command is used to apply the Kubernetes deployment and service manifests. This stage is only executed on the master branch, and is marked as running in the production environment with a specified URL.

## Run Pipeline ## 

To execute the following gitlab-ci.yaml file, you can follow these steps:

Save the code in a file with the name .gitlab-ci.yml.

Open the GitLab CI/CD pipeline for the repository where you want to run the pipeline.

Make sure that the variables mentioned in the pipeline file are defined in the GitLab CI/CD pipeline settings as well. Specifically, you need to define MY_REGISTRY_PASSWORD, SONAR_HOST_URL, SONAR_LOGIN_TOKEN, zone, project_id, and load-balancer-ip-address variables.

The pipeline consists of five stages: build, test, analyze, configure, and deploy. Each stage has a specific set of instructions to be executed.

The build stage builds a Docker image of the PHP application and pushes it to a Docker registry. The test stage runs any automated tests for the application. The analyze stage runs a SonarQube analysis on the code. The configure stage configures kubectl to work with the Kubernetes cluster. Finally, the deploy stage deploys the PHP application to the Kubernetes cluster.

The pipeline only runs when changes are pushed to the master branch.

To manually run the pipeline, click on the "CI/CD" button on the GitLab repository page, and then click "Run Pipeline".